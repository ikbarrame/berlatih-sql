Nama : IKBAR NUR MUHAMMAD

Soal 1 Membuat Database

create database myshop;

Soal 2 Membuat Table di Dalam Database

-table user
 create table users(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

-table items
 create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(50),
    -> stock int(50),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

-table categories
create table categories(
    -> id int(8) auto_increment,
    -> nama varchar(255),
    -> primary key(id)
    -> );

Soal 3 Memasukkan Data pada Table

-users
insert into users(name, email, password)
    -> values
    -> ("John Doe", "john@doe.com", "john123"),
    -> ("Jane Doe", "jane@doe.com", "jenita123")
    -> ;

-categories
insert into categories(nama)
    -> values
    -> ("gadget"),
    -> ("cloth"),
    -> ("men"),
    -> ("women"),
    -> ("branded")
    -> ;

-items
insert into items(name, description, price, stock, category_id)
    -> values
    -> ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", 1),
    -> ("Uniklooh", "baju keren dari brand ternama", "500000", "50", 2),
    -> ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", 1)
    -> ;


Soal 4 Mengambil Data dari Database

a. Mengambil data users

select id, name, email from users;

b. Mengambil data items

-table items yang memiliki harga di atas 1000000 (satu juta)
select *
    -> from items
    -> where price > 1000000;

-table items yang memiliki name serupa atau mirip (like)
select *
    -> from items
    -> where name like '%watch';

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.nama as kategori
    -> from items
    -> inner join categories
    -> on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database

update items
    -> set name = "Sumsang b50", price = "2500000"
    -> where id = 1;

